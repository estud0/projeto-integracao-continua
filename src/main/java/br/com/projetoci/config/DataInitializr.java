package br.com.projetoci.config;

import br.com.projetoci.entity.Role;
import br.com.projetoci.entity.User;
import br.com.projetoci.repository.RoleRepository;
import br.com.projetoci.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            this.createUsers("Fernando Leis", "fernandoleis@gmail.com", passwordEncoder.encode("123456"), "ROLE_ALUNO");
            this.createUsers("Fernando Leis", "admin", passwordEncoder.encode("123456"), "ROLE_ADMIN");
            this.createUsers("Fernando Leis2", "fernandoleis@gmail.com.br", passwordEncoder.encode("123456"), "ROLE_ALUNO");
            this.createUsers("Fernando Leis3", "fernandoleis@outlook.com", passwordEncoder.encode("123456"), "ROLE_ALUNO");
        }
    }

    public void createUsers(String name, String email, String password, String role) {
        Role roleObject = new Role();
        roleObject.setName(role);

        this.roleRepository.save(roleObject);
        User user = new User(name, email, password, Arrays.asList(roleObject));
        userRepository.save(user);
    }
}
