package br.com.projetoci.controller;

import br.com.projetoci.entity.User;
import br.com.projetoci.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Secured("ROLE_ADMIN")
    @GetMapping
    public Page<User> list(@RequestParam("page") int page, @RequestParam("size") int size) {
        return userRepository.findAll(PageRequest.of(page, size));
    }

    @PostMapping
    public User save(@RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping
    public User edit(@RequestBody User user) {
        return userRepository.save(user);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        userRepository.deleteById(id);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<User> detail(@PathVariable Long id) {
        User user = userRepository.findById(id).get();
        return ResponseEntity.ok(user);
    }


}
