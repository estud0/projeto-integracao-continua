package br.com.projetoci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetociApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetociApplication.class, args);
    }

}
